package com.customertimes.circularprogressbar;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends Activity {

    ProgressBar progressBar;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        textView = (TextView) findViewById(R.id.textView);
        new updateProgress().execute();

    }

    private class updateProgress extends AsyncTask<Context, Integer, Void> {

        @Override
        protected Void doInBackground(Context... params) {
            int i = 0;
            while (i < 100) {
                try {
                    Thread.sleep(30);
                    publishProgress(i);
                    i++;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressBar.setProgress(values[0]);
            textView.setText(String.valueOf(values[0]));
        }
    }


}
